export const state = () => ({
  userToken: '',
  user: {},
});

export const mutations = {
  setUserToken(state, token) {
    state.userToken = token;
  },

  setUserInfo(state, user) {
    state.user = user;
  }
};
